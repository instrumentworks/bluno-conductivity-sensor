**Bluno Controlled EC EZO Atlas Scientific Conductivity Sensor Firmware**

This repository contains code for using a Bluno to receive data from an EC EZO conductivity. Inaddition, the code will allow a user to calibrate (clear calibrate, dry calibration and 2 point calibration), set temperature compensation, set a conductivity probes K value and select which measurement from the sensor to display/record (electrical conductivity, total dissolved solids and salinity).

All of the sensors commands have been simplified and are included below. A key of error codes is also supplied.

Additionally, some important global variables will be highlighted to ensure you are aware of how this code functions.

![Wiring Diagram.png](https://bytebucket.org/instrumentworks/iw-ble-bluno-conductivity-sensor/raw/450718983f83c49b2bccc3fd559d771e0010821e/Wiring%20Diagram.png?token=92a61d1b18f31dbe7b3ad9d7e5a926b14497f01e)

**The commands for the Bluno based conductivity sensor, and what is returned in each case.** **_Note: All commands are case sensitive._**


|**Action**        |**Command**  |**Returns**  |**Comments**                              |
|:----------------:|:-----------:|:-----------:|:-----------------------------------------|
|Clear Calibration |CC        |C-OK        |All calibration based commands return C-OK if they are completed correctly.|
|Dry Calibration  |CD        |C-OK        |Probe must be dry before this command is entered.| 
|2-Point calibration low value|CL### 	 |C-OK        |Where ### is the value of the low calibration point. This value must be numerical, either integer or float and entered in micro Siemens per second.|    
|2-Point calibration high value|CH### 	 |C-OK        |Where ### is the value of the high calibration point. This value must be numerical, either integer or float and entered in micro Siemens per second.|
|Temperature correction|T###        |T-OK        |Where ### is the value of the temperature of the liquid. This is a numerical value, either integer or floating point, and entered in degrees Celsius. |
|Entering Probes K value|K###        |K-OK        |Where ### is the value of the temperature of the liquid. This is a numerical value, either integer or floating point, and is between 0.1 and 10.| 
|Measuring electrical conductivity|PE 	 |P-OK       |If entered correctly measurement will change to electrical conductivity and no error message will be received. This is the default value displayed when the sensor is started up.|    
|Measuring total dissolved solids|PT 	 |P-OK       |If entered correctly measurement will change to total dissolved solids and no error message will be received.|
|Measuring salinity|PS 	 |P-OK       |If entered correctly measurement will change to salinity and no error message will be received.|

**Error Codes**


|**Code**    |**Error**                                                             |
|:----------------:|:---------------------------------------------------------------------|
|ERR-1            |First character input is not C, T, P or K|
|ERR-2             |If C or P is first character input, then the second character entered was not C, D, L, H (if C was first character input) or E, T or S (if P was first character input)|
|ERR-3             |A number (integer or floating point) was not entered correctly following CH, CL, K or T.|
|ERR-4             |An error was returned from the sensorSerial communication. This is a larger error and will require debugging.|
|ERR-5             |The EC chip has not understood the command send via the Bluno. This is an error that will require further debugging.|


**Global Variables of Note**


|Variable|Purpose|
|:------:|:------|
|sensorResponse|Empty string used to store incoming software serial data.|
|instruction |Empty string used to store incoming hardware serial data.|
|instructionEnterred |Boolean flag indicating something has entered the hardware serial and been stored in instruction String.|
|cond |Boolean flag indicating that conductivity data will be sent back from the chip. Default true.|
|dissolved |Boolean flag indicating that total dissolved solid data will be sent back from the chip. Default false.|
|s |Boolean flag indicating that salinity data will be sent back from the chip. Default false.|