#include <SoftwareSerial.h>                           //we have to include the SoftwareSerial library, or else we can't use it
#define rx 2                                          //define what pin rx is going to be
#define tx 3                                          //define what pin tx is going to be

SoftwareSerial sensorSerial(rx, tx);                  //sets software serial to run on designated rx and tx pins

String sensorResponse = "";                           //creates an empty string used to store responses fcom the EC EZO sensor
String instruction = "";                              //creates an empty string used to store commands from the app by way of the bluno

int versionNumber = 22;
bool instructionEnterred = false;                     //a flag which indicates an instruction has been enterred and the instruction string is not empty
bool cond = true;                                     //a flag which sets with value the sensor will send to the app. Bluno defaults to sending electrical conductivity value
bool dissolved = false;                               //a flag indicating that total dissolved solids value will be sent to the app. Defaults as false
bool s = false;                                       //a flag indicating that salinity value will be sent to the app. Defaults as false
//bool sg = false;                                    //a flag indicating that specific gravity of sea water value will be sent to the app. Defaults as false and commented out

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);                               //starts serial commincation between app and bluno with baud rate of 115200
  sensorSerial.begin(9600);                           //starts software serial communication between bluno and EC EZO conductivity circuit
  sensorSerial.print("C,?\r");                        //sends command to EC chip inquiring as to whether the chip ic continously seniding data   
  sensorResponse.reserve(30);                         //reserves 30 bytes of data for string sensorResponse
  sensorResponse = sensorSerial.readStringUntil(13);  //checks for response from EC chip. Waits for carriage return to signify when string is complete
  sensorSerial.setTimeout(50);                        //sets a timeout of 50ms for sensorSerial when reading data in serial as strings
  sensorSerial.readString();                          //ensures there is nothing in the software serial buffer
  if (sensorResponse == "?C,0") {                     //if chip is not continously seniding data, this condition is run. If it is conitously sending data, start-up is finished and the code begins to run
   startData();
  }
}

void startData()  {
  sensorSerial.print("C,1\r");                      //sends command to EC chip to send data continously
  sensorSerial.readString();                        //ensures that there is no data remaining in the buffer
}

void stopData() {
  sensorSerial.print("C,0\r");
  sensorSerial.readStringUntil(13);                   //the command is sent to stop transmitting data is sent and the response is discarded
}

//serialEvent runs if anything is detected in the hardware serial (i.e. the bluno receives data from the app)
void serialEvent() {
  instruction = Serial.readStringUntil(13);           //reads the data from the buffer into instruction variable until the carriage return character is detected.
  if (instruction[0] == 'C' || instruction[0] == 'T' || instruction[0] == 'P'  || instruction[0] == 'K' || instruction[0] == 'V') {  //this condition checks that a valid instruction has been entered
    instructionEnterred = true;                       //if the instruction is vaild the instructionEnterred flag is set to true
  } else {  
    Serial.println("ERR-1");                          //if the instruction is valid send the app ERR-1, then
    instruction = "";                                 //clear instruction string 
  }
}

//sendString is a function for sending strings of different values to the EZO EC sensor. Function requires the command string (the atlas scientific command for functions), a floating point value indicating k value 
void sendString(String commandString, float value, String path)  {      //temperature or electrical conductivity and a string called path which informs the function which instruction the command is comming from (C, K or T) 
  if (commandString.endsWith("\r")) {                                   
    sensorSerial.print(commandString);                //if the string has a carriage return, send as is. This is for function which don't require sending a numerical value
  } else  {
    sensorSerial.print(commandString);                //if the string does not have a carriage return, send the string, 
    sensorSerial.print(value);                        //followed by the float value 
    sensorSerial.print("\r");                         //and finally add a carriage return
  }
  String confirm = Serial.readStringUntil(13);        //the sensor will respond
  if (confirm == "*ER") {                             
    Serial.println("ERR-5");                          //if it responds with an error send ERR-5 to the app
  } else {
    Serial.print(path);                               //if it doesn't send the path (C, K or T) followed by -OK to let the user know the action is accomplished
    Serial.println("-OK");
  }
}

//checkFloat is a function designed to ensure that the numerical value required in calibrations, temperature compensation and k values are actual numbers
bool checkFloat() {
  if (instruction[1] == 'L' || instruction[1] == 'H')  {  //this is written this way becuase of previous iterations, a more efficient condition is instruction[0] != 'C'. This is not this way as it has not been tested
    instruction.remove(0,2);                          //if an instruction comes through CL### or CH### it requires two characters to be removed from the string to get access to the numerical part
  } else  {
    instruction.remove(0,1);                          //where as if the instruction comes through T### or K###, it only requires one character to be removed from the string
  }
  int instructionSize = instruction.length();         //instructionSize is used to determine the length of the string and compare to the parts of the string that are valid
  int n = 0;                                          //counter for the number of digits in the string
  int dec = 0;                                        //counter for the number of decimal points in the string
  for (int i = 0; i < instructionSize; i++)  {        //for loop runs a two counters
    if (isdigit(instruction[i]) == true || instruction[i] == '.') {     // if a character in the string is a digit or decimal point, n increases by 1
      if (instruction[i] == '.')  {                         
        dec += 1;                                     //If a character is a decimal point, dec also increases by 1
      }
      n += 1; 
    }
  }
  bool value;                                         //sets up a boolean variable called value
  if (n == instructionSize && dec <= 1) {             //value is true if the instruction size and n is the same length (all characters are digits or decimal points) and if there is 0 or 1 decimal point
    value = true; 
  } else  {
    value = false;                                    //if not value is false
  }
  return value;                                       //checkFloat is not a void function and returns this result back to local variable isFloat in kvalue, temperatureCorrection and Calibration functions
}

void loop() {
  // put your main code here, to run repeatedly:
  //this part of the loop deals with information being sent from the app to the bluno
  if (instructionEnterred == true) {                  //runs if instructioEnterred flag is true (flag is set in serialEvent())
    if (instruction[0] == 'C') {                      //series of if statements designed to parse out what the instruction is and what function to call
      if (instruction[1] == 'C' || instruction[1] == 'D' || instruction[1] == 'L' || instruction[1] == 'H') {   
        calibration();                                //if the correct second instruction for calibration is input, the calibration function will be called
      } else  {
        Serial.println("ERR-2");                      //if not the app will be notified with ERR-2
      }
    } else if (instruction[0] == 'T') {
      temperatureCorrection();                        //T instruction results in the temperatureCorrection function running
    } else if (instruction[0] == 'K') {
      kValue();                                       //K instruction will run the kValue function
    } else if (instruction[0] == 'V') {               //V instruction will return version number of code
      Serial.print("V");
      Serial.println(versionNumber);  
    } else if (instruction[0] == 'P') {               //instruction P results in a check for secondary instruction
      if (instruction[1] == 'E' || instruction[1] == 'T' || instruction[1] == 'S')  { 
        changeParameters();                           //if one of these is the instruction, the function changeParameters will be called.
      } else {
        Serial.println("ERR-2");                      //if no istruction is valid the app will be alerted with ERR-2
      }
    }      
    instructionEnterred = false;                      //once the action has been performed, the instruction flag is reset
    instruction = "";                                 //and the string cleared ready for the next command
  }

  //this part of the loop deals with data being sent from the atlas EZO EC sensor to the bluno
  if (sensorSerial.available() > 0) {                 //if data is in the software serial this condition runs
    sensorResponse = sensorSerial.readStringUntil(13);//string will be read until the carriage return and stored in sensorResponse
    if (isdigit(sensorResponse[0]) == true) {
      returnSensorData();                             //if the first character in the sensor response string is a digit run the returnSensorData function
    }
    else
    {
      if (sensorResponse != "*OK")  {                 //if its not a number, check that the string is not *OK
        Serial.println("ERR-4");                      //if its not *OK send an ERR-4 to the app 
      }
                                                      //if it is *OK do nothing
    }
    sensorResponse = "";                              //clear the sensorResponse string
  }  
}

//calibration function is designed to choose a course of action and apply it using a similar if statement structure to the instruction part of the main loop 
void calibration() {
  stopData();                                       //the command is sent to stop transmitting data 
  if (instruction[1] == 'C') {                      //if statements broken up into by second character in instruction string
    sendString("Cal,clear\r", 0, "C");              //if second character in instruction is C, clear command is sent via sendString function. 0 is sent for the value since nothing is necessary and 
  } else if (instruction[1] == 'D') {               //C is sent as the path string
    sendString("Cal,dry\r", 0, "C");                //if second character in instruction is D, dry calibration command is sent via sendString function. 0 is sent for the value since nothing is necessary and 
  } else if (instruction[1] == 'L' || instruction[1] == 'H') {     //C is sent as the path string
    char calDefine = instruction[1];                //if second character in insrtuction is L or and H, the function gets more complicated, first the L or H is stored in a temporary variable to send it to the appropritate if statement
    bool isFloat = checkFloat();                    //next, the float following CL or CH is checked
    if (isFloat)  {
      float calibration = instruction.toFloat();    //if the string is a valid float, the instruction is converted to float, having been proplery trimmed in the checkFloat function
      if (calDefine == 'L') {                       //the calDefine variable detemines whether to send the string for the low calibration or high calibration
        sendString("Cal,low,", calibration, "C");   //in this case the instruction for low point calibration is sent with the converted float value
      } else if (calDefine =='H') {
        sendString("Cal,high,", calibration, "C");  //in this case the high point calibration command is sent                    
      }
    } else {               
        Serial.println("ERR-3");                      //if the string is not a suitable float, ERR-3 is returned to the app
    }
  }   
  startData();                                        //once calibrated the command to transmit data continously is given again
}

//the temperatureCorrection and kValue functions work in the exact same manor with one difference
void temperatureCorrection() {
  stopData();                                         //data transmission is turned off
  bool isFloat = checkFloat();                        //string is checked to ensure it is possible to turn into a float
  if (isFloat)  {
    float temperature = instruction.toFloat();
    sendString("T,", temperature, "T");               //if it is a float, instruction T, string path T and the float temperature value are sent to sendString function
  } else  {
  Serial.println("ERR-3");                            //if not a suitable float, ERR-3 is returned
  }
  startData();                                        //restart data command
}      

//the one difference between this an temperatureCorrection function is that the range of numbers is set
void kValue() {
  stopData(); 
  bool isFloat = checkFloat();
  if (isFloat)  {
    float k = instruction.toFloat();
    if (k >= 0.1 && k <= 10) {                       //data is only sent to the sendString function if it is from 0.1-10. If not, ERR-6 is returned
      sendString("K,", k, "K");
    } else {
      Serial.println("ERR-6");  
    }
  } else  {
    Serial.println("ERR-3");
  }
  startData();  
}

//changeParameters function is a simple if statement that works by setting a flag for the selected parameter to true, and setting the other two flags to false
void changeParameters() {
  if (instruction[1] == 'E') {
      cond = true;
      dissolved = false;
      s = false;
    } else if (instruction[1] == 'T') {
      cond = false;
      dissolved = true;
      s = false;
    } else if (instruction[1] == 'S') {
      cond = false;
      dissolved = false;
      s = true;
    }
  Serial.println("P-OK");      
}      

void returnSensorData() {
  char sensorResponse_array[30];                        //we make a char array
  char *EC;                                           //char pointer used in string parsing
  char *TDS;                                          //char pointer used in string parsing
  char *SAL;                                          //char pointer used in string parsing
  char *GRAV;                                         //char pointer used in string parsing
  float f_ec;                                         //used to hold a floating point number that is the EC
  float f_tds;
  float f_sal;
  float f_grav;
  
  sensorResponse.toCharArray(sensorResponse_array, 30);   //convert the string to a char array 
  EC = strtok(sensorResponse_array, ",");               //let's pars the array at each comma
  TDS = strtok(NULL, ",");                            //let's pars the array at each comma
  SAL = strtok(NULL, ",");                            //let's pars the array at each comma
  GRAV = strtok(NULL, ",");                           //let's pars the array at each comma

  f_ec= atof(EC);                                     //converts characters to flaoting point numbers
  f_tds= atof(TDS);
  f_sal= atof(SAL);
  f_grav= atof(GRAV);

  //data will be sent to serial if the flag from changeParameters is set to true. Only one flag is true at a time 
  if (cond) {
    Serial.println(f_ec);                                 //this is the EC value
  }

  if (dissolved) {
    Serial.println(f_tds);                                //this is the TDS value
  }

  if (s)  {
    Serial.println(f_sal);                                //this is the salinity value  
  }

  //if (sg) {
    //Serial.println(f_grav);                               //this is the specific gravity
  //}
}  

